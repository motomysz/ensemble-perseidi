const mVel = 0.2;
let particles = [];

function setup() {
  createCanvas(800, 800);
  
  for(let i = 0; i < 20; i++) {
    particles.push(new Particle());
  }
}

function draw() {
  background('#061C30');
  
  for(let i = 0; i < particles.length; i++) {
    particles[i].update();
    particles[i].render();
    
    for(let y = 0; y < particles.length; y++) {
      if(particles[i].check(particles[y].pos) && particles[i] != particles[y]) {
				stroke(255,255,255);
				strokeWeight(0.1);
        line(particles[i].pos.x, particles[i].pos.y, particles[y].pos.x, particles[y].pos.y);
      }
    }
  }
}
class Particle {
    constructor() {
      this.pos = createVector(random(width), random(height));
      this.vel = createVector(random(-mVel, mVel), random(-mVel, mVel));
    }
    
    update() {
      this.pos.add(this.vel); 
      this.bounce();
    }
    
    check(val) {
      const dist = p5.Vector.dist(val, this.pos);
      return dist < 100;
    }
    
    render() {
      fill(255,255,255);
      ellipse(this.pos.x, this.pos.y, 2); 
    }
    
    bounce() {
      if(this.pos.x > width || this.pos.x < 0) {
        this.vel.x *= -1; 
      }
      if(this.pos.y > height || this.pos.y < 0) {
        this.vel.y *= -1; 
      }
    }
  }